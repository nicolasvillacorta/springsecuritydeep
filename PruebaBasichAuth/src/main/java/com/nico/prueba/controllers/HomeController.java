package com.nico.prueba.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("home")
	public String home() {
		return "Todos pueden ver esta pagina, no hace falta estar logeado.";
	}
	
	@GetMapping("admin")
	public String admin() {
		return "Solo el admin puede ver esto";
	}
	
	@GetMapping("any")
	public String any() {
		return "Cualquier usuario logeado puede ver esto";
	}
	
	@GetMapping("manager")
	public String man() {
		return "Solo managers para arriba pueden ver esto.";
	}
	
	@GetMapping("test1")
	public String test1() {
		return "SOY TEST1";
	}
	
	@GetMapping("test2")
	public String test2() {
		return "SOY TEST2";
	}
	
}
