package com.nico.prueba.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		// InMemoryAuthentication significa que nuestros users y pw se van a guardar en memoria cuando se inicie
		// la app. Podria sino usar un UserDetailsService.
		auth.inMemoryAuthentication()
			.withUser("admin").password(passwordEncoder().encode("admin123"))
				.roles("ADMIN").authorities("ACCESS_TEST1", "ACCESS_TEST2")
			.and()
			.withUser("soymanager").password(passwordEncoder().encode("manager"))
				.roles("MANAGER").authorities("ACCESS_TEST1")
			.and()
			.withUser("nico").password(passwordEncoder().encode("12345"))
				.roles("USER");
		
		//Habia dejado ...password("admin123")... pero rompe al no tener un encoder.
	}
	// Hasta aca solo tengo los usuarios, ahora tengo que protejer la app.
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http		
				.authorizeRequests()
//				.anyRequest().authenticated() // Esto permite a cualquier usuario que logee, puede hacer clqr request.
				.antMatchers("/home").permitAll() // Asi clqra sin logear puede ver el home.
				.antMatchers("/any").authenticated() // Cualquiera autenticado puede ver el any.
				.antMatchers("/admin").hasRole("ADMIN") // Solo la gente que es ADMIN puede ver esta page.
				.antMatchers("/manager").hasAnyRole("ADMIN", "MANAGER") // Cualquiera que sea admin o manager
				.antMatchers("/test1").hasAuthority("ACCESS_TEST1")
				.antMatchers("/test2").hasAuthority("ACCESS_TEST2")
				.and()
				.httpBasic(); // Basic Authentication.
				// Para matchear a partir de un caracter de la url; "/home/**".
	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}
