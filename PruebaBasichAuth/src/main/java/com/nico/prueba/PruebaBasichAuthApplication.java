package com.nico.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaBasichAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaBasichAuthApplication.class, args);
	}
	

}
