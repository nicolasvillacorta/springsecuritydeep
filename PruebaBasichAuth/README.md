El SecurityConfiguration (simple), siempre va a tener 2 metodos overrideados.

Dos metodos configure, uno para configurar el DataSource de los usuarios y el otro para los requests. Siempre que 
asigno un rol, o privilegios, tengo que ir al otro a decir que pueden hacer con ese rol o privilegio.

Aunque usa inMemoryAuthentication, la version de Spring Security me obliga a usar un passwordEncoder, sino rompe
por eso tuve que agregar el encoder. Por eso use el BCryptPasswordEncoder de Spring Security.

La diferencia entre granted authorities y roles, es que las authorities/privileges son mas precisos, por ejemplo
READ_ACCESS, WRITE_ACCESS, etc. Los roles son mas generales, ADMIN, USER, etc.
Se puede definir una authority para cada feature de la aplicacion, pero estar creando un rol entero para una sola cosa no es tan copado. Suele ser mejor hacer la autorizacion del servicio basada en permisos.

----------------------------
SSL/HTTPS
Los certificados pueden ser self-signed o comprados, los self-signed deberian usarse solo para motivos de desarrollo, nunca en prod deberian haber certificados autofirmados.

Subi al siguiente repo de github los pasos para hacer el certificado:
	https://github.com/nicolasvillacorta/utilities/tree/master/Certificado%20SSL%20con%20JDK

